﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SonarTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Method1();
            Method2();
            bool result = Login();
            var response = Method3Async(5).Result;

            Console.ReadLine();
        }

        public static async void Method1()
        {
            //var d = new FileStyleUriParser();
            int target = -5;
            int num = 3;

            target = -num;
            target = +num;
        }

        public static void Method2()
        {
            for (int i = 1; i <= 5; i++)
            {
                Console.WriteLine(i);
                if (1 < 3)
                {
                    i = 20;
                }
            }
        }

        public static bool Login()
        {
            string userName = "admin";
            string password = "admin123";
            if (userName == "admin" && password == "admin123")
                return true;
            else
                return false;
        }

        public static Task<object> Method3Async(int i)
        {
            //if (i == 5)
            //{
            //    return "fgf";
            //}
            return null;
        }
    }
}
